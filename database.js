/* eslint-disable @typescript-eslint/no-var-requires */
const { Client } = require('pg');

const client = new Client({
  host: 'localhost',
  user: 'postgres',
  port: 5432,
  password: 'neel1501',
  data: 'postgres',
});
async function main() {
  try {
    await client.connect();
    console.log('Connected');
    await select(`select 
  	count(*)* 100.0 /(
    select 
      count(*) 
    from 
      posts 
    where 
      posttypeid = 1
  ) percentage 
from 
  posts 
where 
  answercount >= 1`);
    await select(`select 
  id, 
  displayname, 
  reputation 
from 
  users 
order by 
  reputation desc 
limit 
  10`);
    await select(`select 
  d, 
  answered 
from 
  (
    select 
      count(answers.id) as answered, 
      to_char(answers.creationdate, 'day') as d 
    from 
      (
        select 
          id, 
          creationdate, 
          parentid 
        from 
          posts 
        where 
          posttypeid = 2
      ) as answers 
      inner join (
        select 
          id, 
          creationdate 
        from 
          posts 
        where 
          posttypeid = 1
      ) as questions on answers.parentid = questions.id 
    where 
      (
        (
          extract(
            epoch 
            from 
              answers.creationdate
          )- extract(
            epoch 
            from 
              questions.creationdate
          )
        )/ 3600
      )< 1 
    group by 
      d
  ) as ans 
where 
  answered =(
    Select 
      max(answered) 
    from 
      (
        select 
          count(answers.id) as answered, 
          to_char(answers.creationdate, 'day') as d 
        from 
          (
            select 
              id, 
              creationdate, 
              parentid 
            from 
              posts 
            where 
              posttypeid = 2
          ) as answers 
          inner join (
            select 
              id, 
              creationdate 
            from 
              posts 
            where 
              posttypeid = 1
          ) as questions on answers.parentid = questions.id 
        where 
          (
            (
              extract(
                epoch 
                from 
                  answers.creationdate
              )- extract(
                epoch 
                from 
                  questions.creationdate
              )
            )/ 3600
          )< 1 
        group by 
          d
      )
  )`);
    await select(`select 
  id, 
  creationdate, 
  upvote 
from 
  posts 
  inner join (
    select 
      postid, 
      count(*) as upvote 
    from 
      votes 
    where 
      votetypeid = 2 
    group by 
      postid
  ) as v on posts.id = v.postid 
where 
  extract(
    year 
    from 
      creationdate
  )= 2015 
  and posttypeid = 1 
order by 
  upvote desc 
limit 
  10`);
    await select(`select 
  count(id) as tot, 
  tags 
from 
  posts 
where 
  tags is not null 
group by 
  tags 
order by 
  tot desc 
limit 
  5`);
    await select(`select 
  extract(
    year 
    from 
      creationdate
  ) as year, 
  count(*) 
from 
  posts 
where 
  posttypeid = 1 
group by 
  year 
order by 
  year`);
    await select(`select 
  posts.id, 
  posts.tags, 
  title 
from 
  posts 
  inner join (
    select 
      tagname, 
      count 
    from 
      tags 
    order by 
      count
  ) as t on posts.tags like concat('%', tagname, '%') 
where 
  posttypeid = 1 
  and extract(
    year 
    from 
      creationdate
  )= 2014 
limit 
  3`);
    await select(`select 
  sum(
    viewcount + answercount + commentcount
  )+ count(id) as total_usage, 
  extract(
    year 
    from 
      creationdate
  ) as year 
from 
  posts 
group by 
  year`);
    await select(`SELECT 
  ut.id, 
  ut.displayname, 
  SUM(
    total_votes + totalposts + ct.totalcomments
  ) AS points 
FROM 
  (
    SELECT 
      comments.userid, 
      COUNT(comments.id)* 3 AS totalcomments 
    FROM 
      comments 
    GROUP BY 
      comments.userid
  ) AS ct 
  INNER JOIN (
    SELECT 
      users.id, 
      users.displayname, 
      SUM(users.upvotes + users.downvotes) AS total_votes, 
      COUNT(posts.owneruserid)* 10 AS totalposts 
    FROM 
      users 
      INNER JOIN posts ON users.id = posts.owneruserid 
    GROUP BY 
      users.id, 
      users.displayname
  ) as ut ON ut.id = ct.userid 
GROUP by 
  ut.id, 
  ut.displayname 
ORDER BY 
  points DESC 
LIMIT 
  5;`);
  } catch (err) {
    console.log(err);
  }
}
main();

async function select(query) {
  try {
    client.query(query, (err, res) => {
      if (!err) {
        console.log(res.rows);
      } else {
        console.log(err.message);
      }
      client.end;
    });
  } catch (err) {
    console.log(err);
  }
}
