Abstract:
   This technical paper provides a detailed exploration of fundamental concepts in the field of databases. It covers topics such as ACID properties, the CAP theorem, joins, aggregations, filters in queries, normalization, indexes, transactions, locking mechanisms, database isolation levels, and triggers which will help us gain a better understanding on how database works and how it helps in data-driven applications.

Introduction:
   Databases play a pivotal role in modern software systems, serving as the backbone for data storage and retrieval.This paper provides an in-depth examination of essential database concepts that every database practitioner and developer should be familiar with.

1. ACID Properties: The full form of ACID is Atomicity,Consistency,Isolation,Durability
   i.Atomicity: Each statement in a transaction is treated as a single unit i.e. the entire statement is executed or nothing will execute.
   ii.Consistency: The changes made through transactions are predictable and predefined.
   iii.Isolation: Ensures that multiple transactions can occur concurrently without leading to the inconsistency of the database state.
   iv.Durability: Ensures that once the transaction has completed execution, the updates and modifications to the database are stored in and written to disk and they persist even if a system failure occurs.

2. CAP Theorem:The CAP theorem states that distributed databases can have at most two of the three properties: consistency, availability, and partition tolerance. As a result, database systems prioritize only two properties at a time.
   i.CA(Consistency and Availability): The system prioritizes availability over consistency and can respond with possibly stale data.
   ii.AP(Availability and Partition Tolerance): The system prioritizes availability over consistency and can respond with possibly stale data.The system can be distributed across multiple nodes and is designed to operate reliably even in the face of network partitions.
   iii.The system prioritizes consistency over availability and responds with the latest updated data.The system can be distributed across multiple nodes and is designed to operate reliably even in the face of network partitions.

3. JOINs:A JOIN clause is used to combine rows from two or more tables, based on a related column between them.
   There are 4 types of joins
   i.INNER JOIN(also known as EQUI JOIN): Returns records that have matching values in both tables
   ii.LEFT OUTER JOIN: Returns all records from the left table, and the matched records from the right table
   iii.RIGHT OUTER JOIN: Returns all records from the right table, and the matched records from the left table
   iv.FULL OUTER JOIN: Returns all records when there is a match in either left or right table

4. Aggregation and Filters:Aggregation means collecting a set of values and returning a single result. There are 5 functions:
   i.COUNT counts how many rows are in a particular column.
   ii.SUM adds together all the values in a particular column.
   iii.MIN return the lowest values in a particular column.
   iv.MAX return the highest values in a particular column.
   v.AVG calculates the average of a group of selected values.

5. Normalization: Normalization is the process of organizing the data in the database.Normalization is used to minimize the redundancy from a relation or set of relations. It is also used to eliminate undesirable characteristics like Insertion, Update, and Deletion Anomalies.The normal form is used to reduce redundancy from database table.

6. Indices: Indexing is a data structure technique to efficiently retrieve records from the database files based on some attributes on which the indexing has been done.
    Indexing is defined based on its indexing attributes. Indexing can be of the following types
i. Primary Index − Primary index is defined on an ordered data file. The data file is ordered on a key field. The key field is generally the primary key of the relation.
ii.Secondary Index − Secondary index may be generated from a field which is a candidate key and has a unique value in every record, or a non-key with duplicate values.
iii.Clustering Index − Clustering index is defined on an ordered data file. The data file is ordered on a non-key field.

Ordered Indexing is of two types −

i.Dense Index-In dense index, there is an index record for every search key value in the database. This makes searching faster but requires more space to store index records itself.
ii.Sparse Index-In sparse index, index records are not created for every search key. An index record here contains a search key and an actual pointer to the data on the disk.

7. Transactions:Transactions are a set of operations used to perform a logical set of work. It is the bundle of all the instructions of a logical operation. A transaction usually means that the data in the database has changed. 

8. Locking Mechanism: Any transaction cannot read or write data until it acquires an appropriate lock on it. There are two types of lock:
i.Shared lock: It is also known as a Read-only lock. In a shared lock, the data item can only read by the transaction.
ii. Exclusive lock:In the exclusive lock, the data item can be both reads as well as written by the transaction.
   There are 4 types of locking protocols available:
i.Simplistic Lock Control:It is the simplest way of locking the data while transaction. Simplistic lock-based protocols allow all the transactions to get the lock on the data before insert or delete or update on it. It will unlock the data item after completing the transaction.
ii.Pre-claiming Lock Protocol:Pre-claiming Lock Protocols evaluate the transaction to list all the data items on which they need locks.First they send a request to DBMS for all the locks of data items.If locks are granted, transaction begins else it allows the transaction to roll back and wait until all locks are granted.
iii.Two-phase locking (2PL):The two-phase locking protocol divides the execution phase of the transaction into three parts - Permission for the locks,acquiring the locks and then releasing the acquired locks.
iv.Strict Two-phase locking (Strict-2PL):The first phase of Strict-2PL is similar to 2PL. In the first phase, after acquiring all the locks, the transaction continues to execute normally. The only difference is that in Strict-2PL, all the locks are released only after the entire transaction is executed.

9. Isolation Levels: Isolation levels define the degree to which a transaction must be isolated from the data modifications made by any other transaction in the database system. A transaction isolation level is defined by the following phenomena: 
i.Dirty Read: When a transaction reads data that has not yet been committed.
ii.Non Repeatable read: Non Repeatable read occurs when a transaction reads the same row twice and gets a different value each time.
iii.Phantom Read – Phantom Read occurs when two same queries are executed, but the rows retrieved by the two, are different.
   Based on these phenomena,the four standard isolation levels are:
i.Read Uncommitted: This is the lowest level of isolation where a transaction can see uncommitted changes made by other transactions. This can result in dirty reads, non-repeatable reads, and phantom reads.
ii.Read Committed: In this isolation level, a transaction can only see changes made by other committed transactions. This eliminates dirty reads but can still result in non-repeatable reads and phantom reads.
iii.Repeatable Read: This isolation level guarantees that a transaction will see the same data throughout its duration, even if other transactions commit changes to the data. However, phantom reads are still possible.
iv.Serializable: This is the highest isolation level where a transaction is executed as if it were the only transaction in the system. All transactions must be executed sequentially, which ensures that there are no dirty reads, non-repeatable reads, or phantom reads.

10. Triggers: A trigger is a stored procedure in a database that automatically invokes whenever a special event in the database occurs. For example, a trigger can be invoked when a row is inserted into a specified table or when specific table columns are updated in simple words a trigger is a collection of SQL statements with particular names that are stored in system memory. It belongs to a specific class of stored procedures that are automatically invoked in response to database server events. Every trigger has a table attached to it.

Conclusion: These properties are essential for managing and designing effective database systems.As technology continues to evolve, a deep knowledge of these concepts will remain invaluable for database practitioners and developers alike.